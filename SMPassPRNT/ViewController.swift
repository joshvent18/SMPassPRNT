//
//  ViewController.swift
//  SMPassPRNT
//
//  Created by Joshua Ventocilla on 6/17/23.
//

import UIKit

class ViewController: UIViewController {
    
    var openURL  = "starpassprnt://"
    var printURL = "https://www.star-m.jp/products/s_print/sdk/passprnt/sample/resource/receipt_sample.pdf"
            
    override func viewDidLoad() {
        super.viewDidLoad()
    }

    @IBAction func butonPressed(_ sender: Any) {
                
        if let url = URL(string: "starpassprnt://v1/print/nopreview?back=smpassprnt%3A%2F%2F&url=https%3A%2F%2Fwww.star-m.jp%2Fproducts%2Fs_print%2Fsdk%2Fpassprnt%2Fsample%2Fresource%2Freceipt_sample.pdf") {
               if UIApplication.shared.canOpenURL(url) {
                   UIApplication.shared.open(url, options: [:])
                   print("OPENED URL")
               }
           }

    }
    
}
